# Gitlab-snippet-cli
Easy to use. Just download releases.zip or build with `go build` . Choose your architecture and use. 
<br>

For example: 

DEVICE | PROGRAM
------------ | -------------
MacBook < 2020 | gsnip-amd64
MacBook > 2020 | gsnip-arm64
Raspberry Pi 3B+ | gsnip-linux-arm7
Raspberry Pi Zero | gsnip-linux-arm6

You can check it with `cat /proc/cpuinfo` (linux)

# How it works?

![Screenshot](just-working.png)
1. Use terminal for it
2. Use -p="False,0,FALSE or True,1,TRUE" for **snippet privacy**. P.S If you didn't write it, default value will be used
3. Use -t="SomeThing" for **Title of snippet**
4. Use -t="Text" for **snippet text**.
5. Use -bc="False,0,FALSE or True,1,TRUE" for **broadcast mode**. P.S It will publish the snippet to gitlab and wait for a response (keyword 'stop'), after it, the snippet will be deleted

Easy: 
`snip -t='My best code' -s='Awesome!'`


# Configuration
- `token` -- secret token from https://gitlab.com/-/profile/personal_access_tokens\ **(p.s Select api, read_api, read_user)**.
- `snippets_are_public_by_default` -- Are snippets public by default? **(public/private)**.

### `config.ini` - config file (DON'T REMOVE IT)
```ini
token = DNSJAndjs21
snippets_are_public_by_default = private
```

## TODO:
- [x] Post snippet
- [x] "Broadcast" snippet
- [ ] List of all snippets
- [ ] Remove snippet
