package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/alyu/configparser"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

type Config struct {
	token  string
	public string
}

type result struct {
	Id string `json:"id"`
}

type file struct {
	Content   string `json:"content"`
	File_path string `json:"file_path"`
}

type snippet struct {
	Title      string `json:"title"`
	Visibility string `json:"visibility"`
	Files      []file `json:"files"`
}

func createConfig(config *configparser.Configuration, Config Config) {
	_, _ = config.Delete("config")

	fmt.Println("Let's create config.ini with your data\n" +
		"Give me your token from https://gitlab.com/-/profile/personal_access_tokens\n" +
		"You must select 'api', 'read_user' and 'read_api'")
	_, _ = fmt.Scanln(&Config.token)
	fmt.Println("Okay, Your token is " + Config.token)
	var publicOrNot string
	fmt.Println("\nAre snippets public by default?\n" +
		"Yes, they are public | No, they are not public (yes/no)\n" +
		"You can choose it everytime you publish a snippet. But if you don't - the default config will help us\n" +
		"Write Yes or No")
	_, _ = fmt.Scanln(&publicOrNot)
	if publicOrNot == "Yes" || publicOrNot == "yes" {
		Config.public = "public"
	} else {
		Config.public = "private"
	}

	section := config.NewSection("config")
	section.Add("token", Config.token)
	section.Add("snippets_are_public_by_default", Config.public)

	_ = os.Remove("config.ini")
	err := configparser.Save(config, "config.ini")
	if err != nil {
		log.Fatal(err)
	}
	_ = os.Remove("config.ini.bak")
}

func removeSnippet(config Config, id int) {
	url := "https://gitlab.com/api/v4/snippets/" + strconv.Itoa(id)

	req, err := http.NewRequest("DELETE", url, nil)
	req.Header.Set("PRIVATE-TOKEN", config.token)
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	if err != nil {
		log.Fatal(err)
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	//fmt.Println("response Status:", resp.Status)
	//fmt.Println("response Headers:", resp.Header)
	//body, _ := ioutil.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))
	fmt.Println("Removed")
}

func createSnippet(config Config, snippet snippet) (id int) {
	url := "https://gitlab.com/api/v4/snippets"

	jsonValue, err := json.Marshal(snippet)
	if err != nil {
		log.Fatal(err)
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonValue))
	req.Header.Set("PRIVATE-TOKEN", config.token)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	//fmt.Println("response Status:", resp.Status)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	id = int(gjson.Get(string(body), "id").Int())
	fmt.Println("URL: " + gjson.Get(string(body), "web_url").String())

	return id
}

func main() {
	configparser.Delimiter = "="

	var Config = Config{}

	config, err := configparser.Read("config.ini")
	if err != nil {
		log.Fatal(err)
	}
	section, err := config.Section("config")
	if err != nil {
		log.Fatal(err)
	} else {
		Config.token = section.ValueOf("token")
		if section.ValueOf("snippets_are_public_by_default") == "public" {
			Config.public = "public"
		} else if section.ValueOf("snippets_are_public_by_default") == "private" {
			Config.public = "private"
		} else {
			createConfig(config, Config)
			os.Exit(1)
		}
	}

	fmt.Println("Hello")

	var snippet = snippet{}
	var broadcast bool
	var file_snippet = file{}

	flag.StringVar(&snippet.Visibility, "p", Config.public, "a bool")
	flag.BoolVar(&broadcast, "bc", false, "a bool")
	flag.StringVar(&snippet.Title, "t", "", "a text")
	file_snippet.File_path = "Code"
	flag.StringVar(&file_snippet.Content, "s", "", "a text")
	flag.Parse()
	snippet.Files = append(snippet.Files, file_snippet)

	if snippet.Title == "" || file_snippet.Content == "" {
		fmt.Println("Usage:\n" +
			"[CREATE]\n" +
			"snip -p=false -bc=true -t='some text'\n" +
			"Where\n" +
			"[   ] '-p' is 'publish snippet or no' - It can be False,0,FALSE or True,1,TRUE. If you didn't write it, default value will be used\n" +
			"[   ] '-bc' - broadcast mode. It will publish the snippet to gitlab and wait for a response (keyword 'stop'), after it, the snippet will be deleted\n" +
			"[ ! ] '-t' - Title of snippet. \n" +
			"[ ! ] '-s' - snippet\n\n" +
			"Easy: snip -t='My best code' -s='Awesome!'")
	} else {
		fmt.Println("Okay: \n" +
			"Is snippet public?: " + snippet.Visibility + "\n" +
			"Is broadcast mode turned on?: " + strconv.FormatBool(broadcast) + "\n" +
			"Title: " + snippet.Title + "\n" +
			"Lenght of snippet: " + strconv.Itoa(len(file_snippet.Content)))
		id := createSnippet(Config, snippet)
		if broadcast == true {
			var stop_broadcast = ""
			for stop_broadcast != "stop" {
				fmt.Println("Broadcasting... " +
					"\nWrite 'stop' to stop this and remove snippet")
				_, _ = fmt.Scanln(&stop_broadcast)
			}
			removeSnippet(Config, id)
		}

	}

}
