module gitlab.com/SPUZ_/gitlab-snippet-cli

go 1.16

require (
	github.com/alyu/configparser v0.0.0-20191103060215-744e9a66e7bc
	github.com/tidwall/gjson v1.6.8
)
